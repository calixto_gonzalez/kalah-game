package com.kalah.demo.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kalah.demo.dto.MakeMoveDTO;
import com.kalah.demo.exception.Error;
import com.kalah.demo.exception.GameInvalidMovementException;
import com.kalah.demo.exception.GameInvalidPlayerException;
import com.kalah.demo.exception.GameNotFoundException;
import com.kalah.demo.exception.GameOverException;
import com.kalah.demo.model.Game;
import com.kalah.demo.model.PlayerType;
import com.kalah.demo.repository.GameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GameServiceImplTest {

    @Mock
    private GameRepository gameRepository;
    @InjectMocks
    private GameServiceImpl gameService;

    @Captor
    private ArgumentCaptor<Game> captor;


    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(gameService, "emptyCaptureVariant", true);
    }

    @Test
    void findById() {
    }

    @Test
    void createGame() {
    }

    @Test
    @DisplayName("North Player should play again because last pebble was in his mancala")
    void makeMove_NORTH_PLAYER_shouldPlayAgainBecauseLastPebbleWasHisMancala() {
        List<Integer> listOfPebblesInOrder = Arrays.asList(4, 4, 4, 4, 4, 4, 0, 4, 4, 0, 5, 5, 5, 1);
        Map<Integer, Integer> pebblesAfterPlay = getBoardAfterMove(listOfPebblesInOrder);
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(10);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        Game standardGame = getGame("standardGame");
        doReturn(Optional.of(standardGame)).when(gameRepository).findById("1");
        gameService.makeMove("1", makeMove);
        verify(gameRepository).save(captor.capture());
        Game game = captor.getValue();
        assertEquals(pebblesAfterPlay, game.getBoard());
        assertEquals(PlayerType.NORTH_PLAYER, game.getStatus().getWhoPlays());
        assertFalse(game.getStatus().isGameOver());
    }

    @Test
    @DisplayName("South Player should play because North Player last pebble was not his mancala")
    void makeMove_SOUTH_PLAYER_shouldPlayBecauseLastPebbleWasNotInHisMancala() {
        List<Integer> listOfPebblesInOrder = Arrays.asList(4, 4, 4, 4, 4, 4, 0, 4, 0, 5, 5, 5, 5, 0);
        Map<Integer, Integer> pebblesAfterPlay = getBoardAfterMove(listOfPebblesInOrder);
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(9);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        Game standardGame = getGame("standardGame");
        doReturn(Optional.of(standardGame)).when(gameRepository).findById("1");
        gameService.makeMove("1", makeMove);
        verify(gameRepository).save(captor.capture());
        Game game = captor.getValue();
        assertEquals(pebblesAfterPlay, game.getBoard());
        assertEquals(PlayerType.SOUTH_PLAYER, game.getStatus().getWhoPlays());
        assertFalse(game.getStatus().isGameOver());
    }

    @Test
    @DisplayName("Game should be over after movement")
    void makeMove_shouldEndGame() {
        List<Integer> listOfPebblesInOrder = Arrays.asList(0, 0, 0, 0, 0, 0, 38, 0, 0, 0, 0, 0, 0, 10);
        Map<Integer, Integer> pebblesAfterPlay = getBoardAfterMove(listOfPebblesInOrder);
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(13);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        Game almostFinishedGame = getGame("almostFinishedGame");
        doReturn(Optional.of(almostFinishedGame)).when(gameRepository).findById("1");
        gameService.makeMove("1", makeMove);
        verify(gameRepository).save(captor.capture());
        Game game = captor.getValue();
        assertEquals(pebblesAfterPlay, game.getBoard());
        assertTrue(game.getStatus().isGameOver());
    }

    @Test
    @DisplayName("North Player move should skip South player mancala")
    void makeMove_shouldMoveBeyondOppositePlayerMancala() {
        List<Integer> listOfPebblesInOrder = Arrays.asList(1, 1, 7, 7, 1, 2, 1, 2, 2, 7, 0, 8, 7, 2);
        Map<Integer, Integer> pebblesAfterPlay = getBoardAfterMove(listOfPebblesInOrder);
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(11);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        Game skipOppositePlayerMancalaGame = getGame("skipOppositePlayerMancala");
        doReturn(Optional.of(skipOppositePlayerMancalaGame)).when(gameRepository).findById("1");
        gameService.makeMove("1", makeMove);
        verify(gameRepository).save(captor.capture());
        Game game = captor.getValue();
        assertEquals(pebblesAfterPlay, game.getBoard());
        assertFalse(game.getStatus().isGameOver());
    }

    @Test
    @DisplayName("North Player move should take other's player opposite side pebbles")
    void makeMove_shouldTakeOppositeSidePebbles() {
        List<Integer> listOfPebblesInOrder = Arrays.asList(0, 5, 5, 5, 5, 5, 0, 4, 0, 5, 5, 5, 0, 5);
        Map<Integer, Integer> pebblesAfterPlay = getBoardAfterMove(listOfPebblesInOrder);
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(9);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        Game takeOppositePlayerPebblesGame = getGame("takeOppositePlayerPebbles");
        doReturn(Optional.of(takeOppositePlayerPebblesGame)).when(gameRepository).findById("1");
        gameService.makeMove("1", makeMove);
        verify(gameRepository).save(captor.capture());
        Game game = captor.getValue();
        assertEquals(pebblesAfterPlay, game.getBoard());
        assertFalse(game.getStatus().isGameOver());
    }

    @Test
    @DisplayName("South Player isn't his turn so he should not move and should throw GameInvalidMovementException")
    void makeMove_shouldNotMoveNotHisTurnThrowsGameInvalidPlayerException() {
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(9);
        makeMove.setPlayerType(PlayerType.SOUTH_PLAYER);
        Game game = getGame("standardGame");
        doReturn(Optional.of(game)).when(gameRepository).findById("1");
        assertThrows(GameInvalidPlayerException.class, () -> {
            gameService.makeMove("1", makeMove);}, Error.NOT_PLAYERS_TURN.getMessage());
    }

    @Test
    @DisplayName("North Player shouldn't try to move from an invalid place and should throw GameInvalidMovementException")
    void makeMove_shouldNotMoveFromInvalidPlaceThrowsGameInvalidMovementException() {
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(1);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        Game game = getGame("standardGame");
        doReturn(Optional.of(game)).when(gameRepository).findById("1");
        assertThrows(GameInvalidMovementException.class, () -> {
            gameService.makeMove("1", makeMove);}, Error.INVALID_MOVE.getMessage());
    }

    @Test
    @DisplayName("North Player tries to move but game is over and should throw GameOverException")
    void makeMove_shouldNotMoveIfGameIsOverThrowsGameOverException() {
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(9);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        Game game = getGame("gameOver");
        doReturn(Optional.of(game)).when(gameRepository).findById("1");
        assertThrows(GameOverException.class, () -> {
            gameService.makeMove("1", makeMove);}, Error.GAME_OVER.getMessage());
    }

    @Test
    @DisplayName("North Player tries to move but game is not found and should throw GameNotFoundException")
    void makeMove_shouldNotMoveIfGameIsNotFoundThrowsGameNotFoundException() {
        MakeMoveDTO makeMove = new MakeMoveDTO();
        makeMove.setMovementPlace(9);
        makeMove.setPlayerType(PlayerType.NORTH_PLAYER);
        assertThrows(GameNotFoundException.class, () -> {
            gameService.makeMove("1", makeMove);}, Error.GAME_NOT_FOUND.getMessage());
    }

    private Map<Integer, Integer> getBoardAfterMove(List<Integer> numbers) {
        Map<Integer, Integer> board = new HashMap<>();
        int i = 1;
        for (Integer number : numbers) {
            board.put(i, number);
            i++;
        }
        return board;
    }

    private Game getGame(String standardGame) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(new ClassPathResource(standardGame + ".json").getInputStream(), Game.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage(), e.getCause());
        }

    }
}