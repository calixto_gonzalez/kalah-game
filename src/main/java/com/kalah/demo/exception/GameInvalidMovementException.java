package com.kalah.demo.exception;

public class GameInvalidMovementException extends RuntimeException {
    public GameInvalidMovementException(String message) {
        super(message);
    }
}
