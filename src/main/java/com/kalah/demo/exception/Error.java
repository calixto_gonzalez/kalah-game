package com.kalah.demo.exception;

public enum Error {
    GAME_NOT_FOUND("Game not found"),
    GAME_OVER("Game is over you can't make anymore moves"),
    INVALID_MOVE("Either the selected place has no pebbles or it is an invalid move for this player"),
    NOT_PLAYERS_TURN("It is not your turn");

    private final String message;

    Error(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
