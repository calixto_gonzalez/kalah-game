package com.kalah.demo.exception;

public class GameOverException extends RuntimeException {
    public GameOverException(String s) {
        super(s);
    }
}
