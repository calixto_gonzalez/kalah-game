package com.kalah.demo.exception;

import com.kalah.demo.dto.ExceptionResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionsHandler {
    Logger logger = LoggerFactory.getLogger(ExceptionsHandler.class);
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(value = {GameInvalidMovementException.class})
    protected ResponseEntity<ExceptionResponseDTO> handlerInvalidMovementException(GameInvalidMovementException ex, HttpServletRequest request) {
        logger.error("Request: " + request.getRequestURL() + " exception: " + ex.getMessage());
        ExceptionResponseDTO error = new ExceptionResponseDTO(LocalDateTime.now(),"Invalid Movement Error", ex.getMessage(), 400, request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(value = GameInvalidPlayerException.class)
    protected ResponseEntity<ExceptionResponseDTO> handlerInvalidPlayerException(GameInvalidPlayerException ex, HttpServletRequest request) {
        logger.error("Request: " + request.getRequestURL() + " exception: " + ex.getMessage());
        ExceptionResponseDTO error = new ExceptionResponseDTO(LocalDateTime.now(), "Invalid Player Error", ex.getMessage(), 400, request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(value = GameOverException.class)
    protected ResponseEntity<ExceptionResponseDTO> handlerGameOverException(GameOverException ex, HttpServletRequest request) {
        logger.error("Request: " + request.getRequestURL() + " exception: " + ex.getMessage());
        ExceptionResponseDTO error = new ExceptionResponseDTO(LocalDateTime.now(), "Game Over Error", ex.getMessage(), 400, request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(value = GameNotFoundException.class)
    protected ResponseEntity<ExceptionResponseDTO> handlerGameNotFoundException(GameNotFoundException ex, HttpServletRequest request) {
        logger.error("Request: " + request.getRequestURL() + " exception: " + ex.getMessage());
        ExceptionResponseDTO error = new ExceptionResponseDTO(LocalDateTime.now(), ex.getMessage(), "Game Over Error", 400, request.getRequestURI());
        return ResponseEntity.badRequest().body(error);
    }


}
