package com.kalah.demo.exception;

public class GameInvalidPlayerException extends RuntimeException {
    public GameInvalidPlayerException(String message) {
        super(message);
    }
}
