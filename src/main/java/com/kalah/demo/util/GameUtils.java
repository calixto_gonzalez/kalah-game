package com.kalah.demo.util;

import com.kalah.demo.model.PlayerType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameUtils {
    public static final List<Integer> BIG_PITS = Arrays.asList(7, 14);
    public static final int DEFAULT_NUMBER_OF_PEBBLES = 4;
    public static Map<Integer, Integer> TRADITIONAL_BOARD = getTraditionalBoard();
    public static List<PlayerType> PLAYER_TYPES = Arrays.asList(PlayerType.NORTH_PLAYER, PlayerType.SOUTH_PLAYER);
    public static final int END_OF_BOARD = 14;
    public static final int START_PLACE = 1;


    public static Map<Integer, Integer> getTraditionalBoard() {
        Map<Integer, Integer> board = new HashMap<>();
        for (int i = 1; i <= 14; i++) {
            if (!BIG_PITS.contains(i)) {
                board.put(i, DEFAULT_NUMBER_OF_PEBBLES);
            } else {
                board.put(i, 0);
            }
        }
        return board;
    }

    ;
}
