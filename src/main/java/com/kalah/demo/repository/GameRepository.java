package com.kalah.demo.repository;

import com.kalah.demo.model.Game;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GameRepository extends MongoRepository<Game, Long> {
    public Optional<Game> findById(String id);
}
