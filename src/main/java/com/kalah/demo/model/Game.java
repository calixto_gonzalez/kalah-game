package com.kalah.demo.model;

import com.kalah.demo.util.GameUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Data
@Document
@ApiModel("Game")
public class Game {


    @Id
    @ApiModelProperty(name = "id", position = 1, example = "5ef901e1f0554e4fd7ae8586")
    public String id;

    public Game() {
        Player northPlayer = new Player(PlayerType.NORTH_PLAYER);
        Player southPlayer = new Player(PlayerType.SOUTH_PLAYER);
        this.players = Arrays.asList(northPlayer, southPlayer);
        this.status = new GameStatus();
        this.board = GameUtils.TRADITIONAL_BOARD;
    }
    @ApiModelProperty(notes = "List of players min and max 2", example = "[{\"playerType\":\"NORTH_PLAYER\",\"allowedMovements\":[8,9,10,11,12,13],\"pitToSkip\":7,\"bigPit\":14,\"lastPlace\":13,\"startingPlace\":8,\"score\":24},{\"playerType\":\"SOUTH_PLAYER\",\"allowedMovements\":[1,2,3,4,5,6],\"pitToSkip\":14,\"bigPit\":7,\"lastPlace\":6,\"startingPlace\":1,\"score\":24}]")
    @Size(min = 2, max = 2)
    public List<Player> players;
    @ApiModelProperty(notes = "Game status")
    public GameStatus status;
    @ApiModelProperty(notes = "Board of the game consists in places and its respective amount of pebbles", example = "{\"1\":4,\"2\":4,\"3\":4,\"4\":4,\"5\":4,\"6\":4,\"7\":0,\"8\":4,\"9\":4,\"10\":4,\"11\":4,\"12\":4,\"13\":4,\"14\":0}")
    Map<Integer, Integer> board;
}
