package com.kalah.demo.model;

public enum PlayerType {
    SOUTH_PLAYER, NORTH_PLAYER;
}
