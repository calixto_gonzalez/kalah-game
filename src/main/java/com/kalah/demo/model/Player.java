package com.kalah.demo.model;

import com.kalah.demo.util.GameUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@ApiModel(description = "Player of game")
public class Player {
    public Player(PlayerType playerType) {
        this.playerType = playerType;
        this.score = GameUtils.TRADITIONAL_BOARD.values().stream().reduce(Integer::sum).get()/2;
        if (PlayerType.NORTH_PLAYER.equals(playerType)) {
            this.allowedMovements = Movement.NORTH_PLAYER.getValue();
            this.lastPlace = Collections.max(this.allowedMovements);
            this.startingPlace = Collections.min(this.allowedMovements);
            this.bigPit = this.lastPlace + 1;
            this.pitToSkip = this.bigPit / 2;
        } else {
            this.allowedMovements = Movement.SOUTH_PLAYER.getValue();
            this.lastPlace = Collections.max(this.allowedMovements);
            this.startingPlace = Collections.min(this.allowedMovements);
            this.bigPit = this.lastPlace + 1;
            this.pitToSkip = this.bigPit * 2;
        }
    }

    @ApiModelProperty(notes = "It can only be either SOUTH_PLAYER or NORTH_PLAYER", example = "SOUTH_PLAYER")
    private PlayerType playerType;
    @ApiModelProperty(notes= "A list of allowed movements that are different depending on the player", example = "[1,2,3,4,5,6]")
    private List<Integer> allowedMovements;
    @ApiModelProperty(notes = "Pit that this player has to skip", example = "14")
    private int pitToSkip;
    @ApiModelProperty(notes = "Big pit of this player", example = "7")
    private int bigPit;
    @ApiModelProperty(notes = "Last place this player can make a move from", example = "6")
    private int lastPlace;
    @ApiModelProperty(notes = "Starting place this player can make a move from", example = "1")
    private int startingPlace;
    @ApiModelProperty(notes = "Score of this player", example = "24")
    private int score;
}
