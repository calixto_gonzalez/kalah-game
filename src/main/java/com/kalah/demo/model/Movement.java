package com.kalah.demo.model;

import java.util.Arrays;
import java.util.List;

public enum Movement {
    SOUTH_PLAYER(1,2,3,4,5,6),
    NORTH_PLAYER(8,9,10,11,12,13);

    private final List<Integer> allowedMovements;

    public List<Integer> getValue() {
        return allowedMovements;
    }

    Movement(Integer... integers) {
        this.allowedMovements = Arrays.asList(integers);
    }
}
