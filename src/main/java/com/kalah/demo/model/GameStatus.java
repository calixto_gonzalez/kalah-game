package com.kalah.demo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel
public class GameStatus {

    public GameStatus() {
        this.whoPlays = PlayerType.values()[((int) ( Math.random() * 2))];
        this.gameOver = false;
    }


    public GameStatus(PlayerType whoPlays) {
        this.whoPlays = whoPlays;
    }

    @ApiModelProperty(notes = "Determines the player that plays next", example = "NORTH_PLAYER")
    public PlayerType whoPlays;
    @ApiModelProperty(notes = "Whether or not the game is over", example = "false")
    public boolean gameOver;
}
