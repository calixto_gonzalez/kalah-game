package com.kalah.demo.service;

import com.kalah.demo.dto.MakeMoveDTO;
import com.kalah.demo.model.Game;

public interface GameService {
    /**
     * Finds game by id
     * @param id id
     * @return game
     */
    Game findById(String id);

    /**
     * Creates game
     * @return created game
     */
    Game createGame();

    /**
     * Makes move
     * @param id id
     * @param makeMove dto with movement and player
     * @return game after movement
     */
    Game makeMove(String id, MakeMoveDTO makeMove);
}
