package com.kalah.demo.service.impl;

import com.kalah.demo.dto.MakeMoveDTO;
import com.kalah.demo.exception.Error;
import com.kalah.demo.exception.GameInvalidMovementException;
import com.kalah.demo.exception.GameInvalidPlayerException;
import com.kalah.demo.exception.GameNotFoundException;
import com.kalah.demo.exception.GameOverException;
import com.kalah.demo.model.Game;
import com.kalah.demo.model.GameStatus;
import com.kalah.demo.model.Player;
import com.kalah.demo.model.PlayerType;
import com.kalah.demo.repository.GameRepository;
import com.kalah.demo.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.kalah.demo.util.GameUtils.END_OF_BOARD;
import static com.kalah.demo.util.GameUtils.PLAYER_TYPES;
import static com.kalah.demo.util.GameUtils.START_PLACE;

@Service
public class GameServiceImpl implements GameService {
    Logger logger = LoggerFactory.getLogger(GameServiceImpl.class);
    private static final Integer EMPTY_PEBBLES_FROM_PLACE = 0;
    private static final Integer ONE_PEBBLE = 1;
    @Value("${empty.capture.variant}")
    private boolean emptyCaptureVariant;


    private final GameRepository gameRepository;

    public GameServiceImpl(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public Game findById(String id) {
        logger.info("Trying to find game by id: " + id);
        Optional<Game> optionalGame = gameRepository.findById(id);
        if (optionalGame.isPresent()) {
            return optionalGame.get();
        } else {
            throw new GameNotFoundException(Error.GAME_NOT_FOUND.getMessage());
        }
    }

    @Override
    public Game createGame() {
        Game game = new Game();
        return gameRepository.save(game);
    }

    @Override
    public Game makeMove(String id, MakeMoveDTO makeMove) {
        logger.info("Trying to find game by id: " + id);
        Optional<Game> optionalGame = gameRepository.findById(id);
        if (optionalGame.isPresent()) {
            Game game = optionalGame.get();
            logger.info("Game found by id: " + id);
            if (!game.getStatus().isGameOver()) {
                logger.info("Trying to pebbles from: " + makeMove.getMovementPlace());
                PlayerType playerType = makeMove.getPlayerType();
                if (playerType.equals(game.getStatus().getWhoPlays())) {
                    Player actualPlayer = game.getPlayers().stream().filter(player -> player.getPlayerType().equals(playerType)).findFirst().get();
                    boolean isValidMovement = actualPlayer.getAllowedMovements().contains(makeMove.getMovementPlace());
                    boolean placeHasPebbles = game.getBoard().get(makeMove.getMovementPlace()) != 0;
                    if (isValidMovement && placeHasPebbles) {
                        logger.info("Valid movement making move.");
                        makePlayerMove(game, makeMove, actualPlayer);
                        setNewGameStatus(game, actualPlayer);
                        return gameRepository.save(game);
                    } else {
                        throw new GameInvalidMovementException(Error.INVALID_MOVE.getMessage());
                    }
                } else {
                    throw new GameInvalidPlayerException(Error.NOT_PLAYERS_TURN.getMessage());
                }

            } else {
                throw new GameOverException(Error.GAME_OVER.getMessage());
            }
        } else {
            throw new GameNotFoundException(Error.GAME_NOT_FOUND.getMessage());
        }

    }

    /**
     * Sets new game status after movement
     * @param game game
     * @param player player
     */
    private void setNewGameStatus(Game game, Player player) {
        int scoreWithoutBigPit = getScoreWithoutBigPit(game, player);
        if (scoreWithoutBigPit == 0) {
            game.setBoard(cleanBoard(game));
            game.setStatus(new GameStatus(game.getStatus().getWhoPlays(), true));
        } else {
            game.setStatus(new GameStatus(game.getStatus().getWhoPlays(), false));
        }
        player.setScore(scoreWithoutBigPit + game.getBoard().get(player.getBigPit()));
    }

    /**
     * Cleans board
     * @param game game
     * @return cleaned board
     */
    private Map<Integer, Integer> cleanBoard(Game game) {
        Map<Integer, Integer> cleanBoard = new HashMap<>();
        Map<Integer, Integer> actualBoard = game.getBoard();
        for (int i = 1; i <= 14; i++) {
            cleanBoard.put(i,EMPTY_PEBBLES_FROM_PLACE);
        }
        for (Player player : game.getPlayers()) {
            int scoreWithoutBigPit = getScoreWithoutBigPit(game, player);
            cleanBoard.put(player.getBigPit(), actualBoard.get(player.getBigPit()) + scoreWithoutBigPit);
        }
        return cleanBoard;
    }


    /**
     * Gets the score without considering players mancalas
     * @param game game
     * @param player player
     * @return score without considering mancalas
     */
    private int getScoreWithoutBigPit(Game game, Player player) {
        int sum = 0;
        for (int i = player.getStartingPlace(); i <= player.getLastPlace(); i++) {
            sum += game.getBoard().get(i);
        }
        return sum;
    }

    /**
     * Make player move from determined place
     * @param game game
     * @param moveDTO DTO with movement
     * @param actualPlayer the actual player that makes the move
     */
    private void makePlayerMove(Game game, MakeMoveDTO moveDTO, Player actualPlayer) {
        Map<Integer, Integer> board = game.getBoard();
        int subtractedPebbles = board.get(moveDTO.getMovementPlace());
        int startingPlaceToMovePebbles = moveDTO.getMovementPlace() + 1;
        board.put(moveDTO.getMovementPlace(), EMPTY_PEBBLES_FROM_PLACE);
        int i = startingPlaceToMovePebbles;
        int subtractedPebblesCopy = subtractedPebbles;
        do {
            if (i != actualPlayer.getPitToSkip()) {
                Integer pebblesBeforeInsertion = board.get(i);
                if (emptyCaptureVariant && pebblesBeforeInsertion == 0 && subtractedPebblesCopy == 1 && actualPlayer.getAllowedMovements().contains(i)) {
                    logger.info("Taking all pebbles from opposite side");
                    int oppositeSideOfBoard = getOppositeSideOfTheBoard(actualPlayer.getBigPit(), actualPlayer.getBigPit() - i);
                    board.put(actualPlayer.getBigPit(),board.get(actualPlayer.getBigPit()) + board.get(oppositeSideOfBoard) + ONE_PEBBLE);
                    board.put(i, EMPTY_PEBBLES_FROM_PLACE);
                    board.put(oppositeSideOfBoard, EMPTY_PEBBLES_FROM_PLACE);
                } else {
                    board.put(i, pebblesBeforeInsertion + ONE_PEBBLE);
                }
                subtractedPebblesCopy --;
            }
            i = moveIndex(i, 1);
        }while(subtractedPebblesCopy != 0);
        if (moveDTO.getMovementPlace() + subtractedPebbles != actualPlayer.getBigPit()) {
            game.setStatus(new GameStatus(PLAYER_TYPES.stream().filter(playerType -> !playerType.equals(actualPlayer.getPlayerType())).findFirst().get()));
        }
        game.setBoard(board);
    }

    /**
     * Gets the opposite side of the board
     * @param bigPit Big pit position
     * @param moveOnTheOppositeSide how many positions would you like to move
     * @return index of the opposite position
     */
    private int getOppositeSideOfTheBoard(int bigPit, int moveOnTheOppositeSide) {

        return moveIndex(bigPit, moveOnTheOppositeSide);
    }

    /**
     * Moves index to "complete" circle
     * @param start start from which to move
     * @param positionsToMove amount of positions you want to move
     * @return new index
     */
    private int moveIndex(int start, int positionsToMove) {
        do {
            if (start == END_OF_BOARD) {
                start = START_PLACE;
            } else {
                start += 1;
            }

            positionsToMove--;
        } while(positionsToMove > 0);
        return start;
    }
}
