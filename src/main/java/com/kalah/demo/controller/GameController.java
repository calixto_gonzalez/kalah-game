package com.kalah.demo.controller;

import com.kalah.demo.dto.MakeMoveDTO;
import com.kalah.demo.model.Game;
import com.kalah.demo.service.GameService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@ApiOperation(value= "/game")
@RequestMapping("/game")
public class GameController {
    @Autowired
    private GameService gameService;
    Logger logger = LoggerFactory.getLogger(GameController.class);

    @ApiResponses(value = {@ApiResponse(code = 200, message = "Gets game by id")})
    @GetMapping("/{id}")
    public ResponseEntity<Game> getGameById(@PathVariable("id") String id) {
        return ResponseEntity.ok(gameService.findById(id));
    }


    @ApiResponses(value = {@ApiResponse(code = 200, message = "Creates a game")})
    @GetMapping
    public ResponseEntity<Game> createGame() {
        logger.info("Getting new game");
        return ResponseEntity.ok(gameService.createGame());
    }
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Makes a move validating dto first")})
    @PutMapping("/{id}")
    public ResponseEntity<Game> makeMove(@PathVariable("id") String id, @RequestBody @Valid MakeMoveDTO makeMove) {
        logger.info("Tryin to move on place: "+ makeMove.movementPlace);
        return ResponseEntity.ok(gameService.makeMove(id, makeMove));
    }


}
