package com.kalah.demo.dto;

import com.kalah.demo.model.PlayerType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class MakeMoveDTO {
    @NotNull
    @ApiModelProperty(notes = "Movement place from where to move", example = "1")
    public Integer movementPlace;
    @NotNull
    @ApiModelProperty(notes = "Player which will make the move", example = "SOUTH_PLAYER")
    public PlayerType playerType;
}
