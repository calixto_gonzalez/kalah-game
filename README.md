### Prerequistes
You need the following software in order to build and run

*  Maven
*  Java 14
*  Docker

### Description
This mancala's game backend it is responsible for returning a new game, making moves over that game and fetching it by id.
For further documentation remember to go to swagger once the application is running.

### Running

After you have cloned the repository open your terminal there and do ` cd kalah-game && mvn clean install && sudo docker build --no-cache -t kalah-image:latest . && sudo docker-compose up` (Linux, for Windows is the same thing without sudo, Mac also commands without sudo) this should get the dockers up and running one with the Mongo Database and the other one with the Kala Game application, if you want to stop the application do Ctrl+C at any time, and it should stop.

### Endpoints
For the endpoints you should download my Postman Collection to try them out [Postman Collection](https://www.getpostman.com/collections/4a7ad58295081d995a2f)
save the json to a file and import from Postman for more information [Postman Docs](https://learning.postman.com/docs/postman/collections/data-formats/#importing-postman-data)

**More details on the endpoints once the application is app up you have go to** [Swagger URL](http://localhost:8080/swagger-ui.html)
